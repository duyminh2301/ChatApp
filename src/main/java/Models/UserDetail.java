/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author beochot
 */
@Entity
@XmlRootElement
public class UserDetail extends User implements Serializable {

    private static final long serialVersionUID = 1L;
    private String firstName;
    private String lastName;
    private String email;
    private String title;
    private String age;

    @OneToOne
    private FileEntry avatar;
    
    public UserDetail(){
        
    }

    public UserDetail(String firstName, String lastName, String email, String title, String age, FileEntry avatar, String username, String password) {
        super(username, password);
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.title = title;
        this.age = age;

        this.avatar = avatar;
    }
    
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public FileEntry getAvatar() {
        return avatar;
    }

    public void setAvatar(FileEntry avatar) {
        this.avatar = avatar;
    }

    
}
