/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.Set;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author beochot
 */
@Entity
@XmlRootElement(name="historyEntry")
public class PrivateMessageEntry extends MessageEntry implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @ManyToOne
    @JoinColumn(name = "targetId")
    private User target;

    public PrivateMessageEntry() {
    }

    public PrivateMessageEntry(User target, String message, FileEntry file, Calendar timeCreated, User origin, Set<User> readUser) {
        super(message, file, timeCreated, origin, readUser);
        this.target = target;
    }
    
    public PrivateMessageEntry(User target) {
        this.target = target;
    }

    public User getTarget() {
        return target;
    }
    @XmlElement
    public void setTarget(User target) {
        this.target = target;
    }
    
}
