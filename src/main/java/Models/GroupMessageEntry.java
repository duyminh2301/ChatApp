/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.Set;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author beochot
 */
@Entity
@XmlRootElement(name="historyEntry")
public class GroupMessageEntry extends MessageEntry implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @ManyToOne
    @JoinColumn(name = "groupId")
    private ChatGroup target;
    public GroupMessageEntry(){
        
    }

    public GroupMessageEntry(ChatGroup target, String message, FileEntry file, Calendar timeCreated, User origin, Set<User> readUser) {
        super(message, file, timeCreated, origin, readUser);
        this.target = target;
    }
    
    public ChatGroup getTarget() {
        return target;
    }
    @XmlElement
    public void setTarget(ChatGroup target) {
        this.target = target;
    }
    
}
