/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Set;
import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author beochot
 */
@Entity
@XmlRootElement
public class MessageEntry extends HistoryEntry implements Serializable {

    private static final long serialVersionUID = 1L;

    private String message;
    @OneToOne
    private FileEntry file;
    public MessageEntry(){
        
    }

    public MessageEntry(String message, FileEntry file, Calendar timeCreated, User origin, Set<User> readUser) {
        super(timeCreated, origin, readUser);
        this.message = message;
        this.file = file;
    }
    
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public FileEntry getFile() {
        return file;
    }

    public void setFile(FileEntry file) {
        this.file = file;
    }
    
    
}
