/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package APIResources;

import Models.User;
import Util.ApplicationUtil;
import java.util.List;
import javax.annotation.security.RolesAllowed;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.hibernate.Session;

/**
 *
 * @author beochot
 */
@Path("/user")
public class UserResource {
    private final Session session = ApplicationUtil.getInstance().getSessionFactory().openSession();


    
    //Endpoint to get all user in database also their profile
    @RolesAllowed({"Admin", "User"})
    @GET
    @Produces(MediaType.APPLICATION_XML)
    public List<User> getAll() {
        List<User> users = session.createCriteria(User.class).list();
        return users;
    }
    /*
    //Get current user role
    @RolesAllowed({"Admin", "User"})
    @Path("/role")
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String getUserInfo(@Context HttpServletRequest request) {
        int user_idx = (int) request.getAttribute("useridx");
        User currentUser = users.get(user_idx);
        return currentUser.getRole();
    }*/
// Promote user to admin .ADMIN only endpoint
            /*
    @RolesAllowed({"Admin"})
    @Path("/promote")
    @POST
    @Consumes(MediaType.APPLICATION_XML)
    @Produces(MediaType.APPLICATION_XML)
    public Response promote(User promoteUser) {
        //Loop through the list to find the user and set role to Admin

        for (User u : users) {
            if (u.equals(promoteUser)) {
                u.setRole("Admin");
                h.save();
                return Response.ok().build();
            }
        }

        return Response.notModified().build();
    }
//Demote someone from admin to user .ADMIN only endpoint
    @RolesAllowed({"Admin"})
    @Path("/demote")
    @POST
    @Consumes(MediaType.APPLICATION_XML)
    @Produces(MediaType.APPLICATION_XML)
    public Response demote(User promoteUser) {
        //Loop through the list to find the user and set role to User
        for (User u : users) {
            if (u.equals(promoteUser)) {
                u.setRole("User");
                h.save();
                return Response.ok().build();
            }
        }

        return Response.notModified().build();
    }
*/
    /*
    //Endpoint to update user profile
    @RolesAllowed({"Admin", "User"})
    @Path("/profile")
    @POST
    @Consumes(MediaType.APPLICATION_XML)
    @Produces(MediaType.APPLICATION_XML)
    public Response setProfile(User u,@Context HttpServletRequest request) {
        int userId = (int) request.getAttribute("userId");
        User currentUser = users.get(user_idx);
        if(!currentUser.equals(u)){
            return Response.notAcceptable(null).build();
        }
        currentUser.setEmail(u.getEmail());
        currentUser.setFirstName(u.getFirstName());
        currentUser.setLastName(u.getLastName());
        currentUser.setTitle(u.getTitle());
        h.save();
        return Response.ok().build();
    }
    //Endpoint to get current user profile
    @RolesAllowed({"Admin", "User"})
    @Path("/profile")
    @GET
    @Produces(MediaType.APPLICATION_XML)
    public Response getProfile(@Context HttpServletRequest request) {
        int user_idx = (int) request.getAttribute("useridx");
        User currentUser = users.get(user_idx);
        
        return Response.ok(currentUser).build();
    }
    //Endpoint to upload profile image for user
    @RolesAllowed({"Admin", "User"})
    @Path("/image/")
    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response uploadAvatar(FormDataMultiPart form, @Context HttpServletRequest request,@Context ServletContext servletContext) {
        String fileName = h.saveFile(form,servletContext);
        if (fileName == null) {
            return Response.status(500).entity(fileName).build();
        }
        System.out.println(fileName);
        int user_idx = (int) request.getAttribute("useridx");
        User originUser = users.get(user_idx);
       
        originUser.setAvatar(fileName);
        h.save();
        return Response.status(200).entity(fileName).build();
    }

*/
}
