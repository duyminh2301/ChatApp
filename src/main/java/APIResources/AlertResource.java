/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package APIResources;

import RequestSchema.AlertRequest;
import RequestSchema.UserRequest;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.annotation.security.RolesAllowed;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * REST Web Service
 *
 * @author minhcao
 */
@Path("alert")
public class AlertResource {
/*
    @Context
    private UriInfo context;
//    private History h = History.getInstance();
    private List<UserRequest> users = h.getUsers();
    private List<AlertRequest> alerts = h.getAlerts();

    /**
     * Creates a new instance of AlertResource
     
    public AlertResource() {
    }

    @RolesAllowed({"Admin"})
    @POST
    @Consumes(MediaType.APPLICATION_XML)
    @Produces(MediaType.APPLICATION_XML)
    public Response sendAlert(AlertRequest alert, @Context HttpServletRequest request) {
      
        String[] userList = alert.getTargetList().replaceAll(" ", "").split(",");

        // compose users to setAsync
        List<UserRequest> composedList = new ArrayList<>();
        Iterator<UserRequest> iterator = users.iterator();
        while (iterator.hasNext()) {
            UserRequest user = iterator.next();
            for (String toBeAlert : userList) {
                System.out.println("user in list " + toBeAlert + "----" + toBeAlert);
                if (user.getUsername().equals(toBeAlert)) {
                    composedList.add(user);
                    break;
                }
            }
        }

        // send alerts
        System.out.println(composedList);
        try {
            for (UserRequest anUser : composedList) {
                h.notify(anUser, alert);
            }
            h.addAlert(alert);
            h.save();
            return Response.ok().build();

        } catch (Exception e) {
            return Response.notAcceptable(null).build();
        }
    }

    @RolesAllowed({"Admin","User"})
    @POST
    @Path("{alertId}")
    @Produces(MediaType.APPLICATION_XML)
    public Response confirmAlert(@Context HttpServletRequest request, @PathParam("alertId") int alertId) {
        int user_idx = (int) request.getAttribute("useridx");
        final UserRequest currentUser = users.get(user_idx);
        try {
            h.getAlerts().get(alertId - 1).getConfirmList().add(currentUser);
            h.save();
            return Response.ok().build();
        } catch(Exception e) {

            return Response.status(500).entity(e).build();
        }
    }
    
    @RolesAllowed({"Admin","User"})
    @GET
    @Produces(MediaType.APPLICATION_XML)
    public List<AlertRequest> getAlert(@Context HttpServletRequest request) {
        int user_idx = (int) request.getAttribute("useridx");
        final UserRequest currentUser = users.get(user_idx);

        List<AlertRequest> composedList = new ArrayList<>();
        for (AlertRequest alert : alerts) {
            String[] userList = alert.getTargetList().replaceAll(" ", "").split(",");
            for (String username : userList) {
                if (currentUser.getUsername().equals(username) && !alert.getConfirmList().contains(currentUser)) {
                    composedList.add(alert);
                }
            }
        }

        return composedList;
    }
    */
}
