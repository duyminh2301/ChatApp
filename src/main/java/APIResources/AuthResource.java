/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package APIResources;

import Models.User;
import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.apache.commons.codec.digest.DigestUtils;
import RequestSchema.HistoryEntryRequest;
import RequestSchema.UserRequest;
import Util.ApplicationUtil;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Response.Status;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

/**
 * REST Web Service
 *
 * @author minhcao
 */
@Path("/auth")
@PermitAll
public class AuthResource {
    //a secret to generate token
    private final String SECRET = "vietnamvodich";
    private ApplicationUtil util = ApplicationUtil.getInstance();
    private Session s = util.getSessionFactory().openSession();

    @Context
    private UriInfo context;

    public UriInfo getContext() {
        return context;
    }

    public void setContext(UriInfo context) {
        this.context = context;
    }


    public AuthResource() {
    }
    //method to register new user
    @PermitAll
    @Path("/register")
    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_XML)
    //Use form params here
    public Response register(@FormParam("username") String username, @FormParam("password") String password) {
        //check user input
        if(username.trim().isEmpty() || password.trim().isEmpty()){
            return Response.status(Status.NOT_ACCEPTABLE).build();
        }
        s.beginTransaction();
        List<User> users =s.createCriteria(User.class).add(Restrictions.eq("username", username)).add(Restrictions.eq("password", password)).list();
        if(users.size()>0){
            return Response.status(Status.NOT_ACCEPTABLE).build();
        }
        //Create new user
        User u = new User();
        u.setUsername(username);
        u.setPassword(password);
        u.setPrivilege("User");
        //Check if user already exist
       
        HistoryEntryRequest notification = new HistoryEntryRequest(new UserRequest("System"), "System", "New User");
        
        util.notify(notification);
        //generate token for that user
        u.setToken(DigestUtils.shaHex(u.getUsername() + SECRET));
        //Add user to database
        s.saveOrUpdate(u);
        s.getTransaction().commit();
        System.out.println(u.getPassword());
        return Response.ok(u.getToken()).build();
    }

    @PermitAll
    @Path("/login")
    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.TEXT_PLAIN)
    public Response login(@FormParam("username") String username, @FormParam("password") String password) {
        if(username.trim().isEmpty() || password.trim().isEmpty()){
            return Response.status(Status.NOT_ACCEPTABLE).build();
        }

        s.beginTransaction();
        List<User> users =s.createCriteria(User.class).add(Restrictions.eq("username", username)).add(Restrictions.eq("password", password)).list();
        
        if(users.size()>0){
            User u = users.get(0);
            u.setToken(DigestUtils.shaHex(u.getUsername() + SECRET));
            s.saveOrUpdate(u);
            s.getTransaction().commit();
            return Response.ok(u.getToken()).build();
        }
        s.getTransaction().commit();
        
        return Response.status(Status.NOT_ACCEPTABLE).build();
    }
    @RolesAllowed({"Admin", "User"})
    @Path("/logout")
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public Response logout(@Context HttpServletRequest request) {
        User u = (User)request.getAttribute("user");
        util.removeAsync(u.getId());
        return Response.ok("Logged out successfully").build();
    }


}
