/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package APIResources;

import Models.GroupMessageEntry;
import Models.User;
import Models.UserDetail;
import Util.ApplicationUtil;
import java.util.List;
import javax.annotation.security.PermitAll;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.hibernate.Session;

/**
 *
 * @author beochot
 */

 
@Path("/test")
public class TestResource {
    private final Session session = ApplicationUtil.getInstance().getSessionFactory().openSession();

    //Endpoint to get all user in database also their profile
    @PermitAll
    @GET
    @Path("/user")
    @Produces(MediaType.APPLICATION_XML)
    public List<User> getUsers() {
        List<User> users = session.createCriteria(User.class).list();
        return users;
    }
    @PermitAll
    @GET
    @Path("/detail")
    @Produces(MediaType.APPLICATION_XML)
    public List<UserDetail> getDetails() {
        List<UserDetail> users = session.createCriteria(UserDetail.class).list();
        return users;
    }
    @PermitAll
    @GET
    @Path("/entry")
    @Produces(MediaType.APPLICATION_XML)
    public List<GroupMessageEntry> getEntries() {
        List<GroupMessageEntry> groupMessages = session.createCriteria(GroupMessageEntry.class).list();
        return groupMessages;
    }
}
