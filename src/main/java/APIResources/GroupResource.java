/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package APIResources;

import Models.ChatGroup;
import Models.User;
import RequestSchema.HistoryEntryRequest;
import RequestSchema.UserRequest;
import Util.ApplicationUtil;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.annotation.security.RolesAllowed;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author beochot
 */
@Path("/group")
public class GroupResource {

    private ApplicationUtil util = ApplicationUtil.getInstance();
    private Session s = util.getSessionFactory().openSession();

    //Return all groups user is in public and private
    @RolesAllowed({"Admin", "User"})
    @GET
    @Produces(MediaType.APPLICATION_XML)
    public Set<ChatGroup> getJoinedGroups(@Context HttpServletRequest request) {
        User u = (User) request.getAttribute("user");

        return u.getSubcriptions();

    }

    //Return all public group exist in database
    @RolesAllowed({"Admin", "User"})
    @Path("/all")
    @GET
    @Produces(MediaType.APPLICATION_XML)
    public List<ChatGroup> getAllGroups() {

        //loop through all groups and get only not private group
        s.beginTransaction();
        List<ChatGroup> groups = s.createCriteria(ChatGroup.class).add(Restrictions.eq("isPrivate", false)).list();
        s.getTransaction().commit();
        return groups;
    }

    //get all users of any request group , requesting user must be in that group
    @RolesAllowed({"Admin", "User"})
    @Path("{param}")
    @GET
    @Produces(MediaType.APPLICATION_XML)
    public Set<User> getGroupUsers(@Context HttpServletRequest request, @PathParam("param") String name) {
        s.beginTransaction();
        List<ChatGroup> groups = s.createCriteria(ChatGroup.class).add(Restrictions.eq("name", name)).list();
        s.getTransaction().commit();
        if (groups.size() > 0) {
            return groups.get(0).getMembers();
        }
        /*
        int user_idx = (int) request.getAttribute("useridx");
        //get current user fron the index gotten when checking token
        UserRequest currentUser = users.get(user_idx);
        List<UserRequest> members = new ArrayList<>();
        //Get all user inside requested group and add to members arraylist
        for (UserRequest u : users) {
            for (GroupRequest g : u.getSubcriptions()) {
                if (g.getName().equals(name)) {
                    members.add(u);
                    break;
                }
            }
        }
        //check if requesting user is inside the group
        if (!members.contains(currentUser)) {
            return null;
        }*/
        return null;
    }

    //Endpoint to create public group
    @RolesAllowed({"Admin", "User"})
    @Path("/createPublic")
    @POST
    @Consumes(MediaType.APPLICATION_XML)
    @Produces(MediaType.TEXT_PLAIN)
    public Response createPublicGroup(@Context HttpServletRequest request, ChatGroup g) {
        System.out.println("Cai quai gi dang xay ra" + g.getName());
        User u = (User) request.getAttribute("user");
        Set<User> users = new HashSet<>();
        users.add(u);
        if (g.getName().isEmpty() || g.getName().trim().isEmpty()) {
            return Response.notAcceptable(null).build();
        }

        g.setIsPrivate(false);
        g.setMembers(users);
        g.setSize(1);
        s.beginTransaction();
        s.save(g);
        s.getTransaction().commit();
        return Response.ok().build();

    }

    //Endpoint to create private group that only members are able to add new member
    //Will be improved in the future to have owner...
    /*
    @RolesAllowed({"Admin", "User"})
    @Path("/createPrivate")
    @POST
    @Consumes(MediaType.APPLICATION_XML)
    @Produces(MediaType.TEXT_PLAIN)
    public Response createPrivateGroup(@Context HttpServletRequest request, GroupRequest g) {
        int user_idx = (int) request.getAttribute("useridx");
        UserRequest currentUser = users.get(user_idx);
        if (g.getName().isEmpty() || g.getName().trim().isEmpty()) {
            return Response.notAcceptable(null).build();
        }
        //set the group to be private
        g.setPrivate(true);
        g.setSize(1);
        //add the group to database and check if it is already in there and respond accordingly
        if (groups.add(g)) {
            currentUser.getSubcriptions().add(g);
            h.save();
            return Response.ok().build();
        }
        //Temporary response.Will be fix to return specified error
        return Response.notAcceptable(null).build();
    }

    //Endpoint to add member to a group
    @RolesAllowed({"Admin", "User"})
    @Path("/addUser/{param}")
    @POST
    @Consumes(MediaType.APPLICATION_XML)
    @Produces(MediaType.TEXT_PLAIN)
    public Response addUsertoGroup(@Context HttpServletRequest request, @PathParam("param") String groupName, List<UserRequest> invitedUsers) {
        GroupRequest g = new GroupRequest(groupName);
        int user_idx = (int) request.getAttribute("useridx");
        UserRequest currentUser = users.get(user_idx);
        //Checking input
        if (groupName.isEmpty() || groupName.trim().isEmpty() || users.isEmpty() || (!currentUser.getSubcriptions().contains(g))) {
            return Response.notAcceptable(null).build();
        }

        int numUser = 0;  //Init number of user that actually join
        //loop through the list of users and check if there are anyone already in the groups
        //Responds error if someone in the list already joined 
        String invitedNames = "";

        for (UserRequest u : users) {
            for (UserRequest invitedUser : invitedUsers) {
                /* if (!users.contains(invitedUser)) {
                    return Response.notAcceptable(null).build();
                }
                //Check find invited users inside arraylist and add new group subscription
                //At the same time check again if user already subscribe to that group
                HistoryEntryRequest notification = new HistoryEntryRequest(new UserRequest("System"), "System", "New Group");
                if (u.equals(invitedUser) && u.getSubcriptions().add(g)) {
                    //Increase the number of user in the group
                    h.notify(u, notification);
                    invitedNames += u.getUsername() + ",";
                    numUser++;
                    break;
                }
            }
        }
        if(invitedNames.isEmpty())
            return Response.notAcceptable(null).build();
        //Find the group in database and set the new size (number of user)
        for (GroupRequest gr : groups) {
            if (gr.equals(g)) {
                gr.setSize(gr.getSize() + numUser);
                break;
            }
        }
        HistoryEntryRequest entry = new HistoryEntryRequest(new UserRequest("System"), groupName, invitedNames.substring(0, invitedNames.length() - 1) + " have been added to the group");
        for (UserRequest u : users) {
            if (u.getSubcriptions().contains(g)) {
                h.notify(u, entry);
            }
        }
        h.addGroupEntry(entry);
        h.save();
        return Response.ok().build();
    }
     */
    //Endpoint to join PUBLIC group 
    @RolesAllowed({"Admin", "User"})
    @Path("/join")
    @POST
    @Consumes(MediaType.APPLICATION_XML)
    @Produces(MediaType.TEXT_PLAIN)
    public Response join(ChatGroup jg, @Context HttpServletRequest request) {
        //Check input validity... empty input, space input

        if (jg.getName().isEmpty() || jg.getName().trim().isEmpty()) {
            return Response.notAcceptable(null).build();
        }
        User currentUser = (User) request.getAttribute("user");
        s.beginTransaction();
        List<ChatGroup> groups = s.createCriteria(ChatGroup.class).add(Restrictions.eq("isPrivate", false)).add(Restrictions.eq("name", jg.getName())).list();
        HistoryEntryRequest entry = new HistoryEntryRequest(new UserRequest("System"), jg.getName(), currentUser.getUsername() + " has joined the group");
        if (groups.size() > 0) {
            ChatGroup g = groups.get(0);
            if(g.getMembers().add(currentUser)){
                g.setSize(g.getSize() + 1);
                for(User u:g.getMembers()){
                    util.notifyUser(u.getId(), entry);
                }
                s.saveOrUpdate(g);
            }
            s.getTransaction().commit();
            return Response.ok().build();
        }
        s.getTransaction().commit();
        return Response.notAcceptable(null).build();
    }

    //This is similar to join,syntax is the same except it is possible to leave private Group
    @RolesAllowed({"Admin", "User"})
    @Path("/leave")
    @POST
    @Consumes(MediaType.APPLICATION_XML)
    @Produces(MediaType.TEXT_PLAIN)
    public Response leave(ChatGroup lg, @Context HttpServletRequest request) {
        if (lg.getName().isEmpty() || lg.getName().trim().isEmpty()) {
            return Response.notAcceptable(null).build();
        }
        User currentUser = (User) request.getAttribute("user");
        s.beginTransaction();
        List<ChatGroup> groups = s.createCriteria(ChatGroup.class).add(Restrictions.eq("isPrivate", false)).add(Restrictions.eq("name", lg.getName())).list();
        HistoryEntryRequest entry = new HistoryEntryRequest(new UserRequest("System"), lg.getName(), currentUser.getUsername() + " has joined the group");
        if (groups.size() > 0) {
            ChatGroup g = groups.get(0);
            if (g.getMembers().remove(currentUser)) {
                g.setSize(g.getSize() - 1);
                for(User u:g.getMembers()){
                    util.notifyUser(u.getId(), entry);
                }
                s.saveOrUpdate(g);

            }
            s.getTransaction().commit();

            return Response.ok().build();
        }
        //HistoryEntryRequest entry = new HistoryEntryRequest(new UserRequest("System"), lg.getName(), currentUser.getUsername() + " has left the group");
        s.getTransaction().commit();
        return Response.notAcceptable(null).build();
    }

}
