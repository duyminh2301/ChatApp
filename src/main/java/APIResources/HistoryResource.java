/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package APIResources;

import Models.ChatGroup;
import Models.GroupMessageEntry;
import Models.PrivateMessageEntry;
import Models.User;
import Util.ApplicationUtil;
import java.util.List;
import javax.annotation.security.RolesAllowed;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author beochot
 */
@Path("/history")
public class HistoryResource {

    private ApplicationUtil util = ApplicationUtil.getInstance();
    private Session s = util.getSessionFactory().openSession();
    //Endpoint to get all private user to user chat history of requestedtarget user
    @RolesAllowed({"Admin", "User"})
    @Path("/@{param}")
    @GET
    @Produces(MediaType.APPLICATION_XML)
    public List<PrivateMessageEntry> getPrivateHistory(@PathParam("param") String privateTarget, @Context HttpServletRequest request) {
        User originUser = (User) request.getAttribute("user");
        s.beginTransaction();
        List<User> users =s.createCriteria(User.class).add(Restrictions.eq("username", privateTarget)).list();
        if(users.size()<1){
            return null;
        }
        User targetUser =users.get(0);
        Criterion c1= Restrictions.and(Restrictions.eq("origin", originUser),Restrictions.eq("target", targetUser));
        Criterion c2= Restrictions.and(Restrictions.eq("origin", targetUser),Restrictions.eq("target", originUser));
        List<PrivateMessageEntry> entries = s.createCriteria(PrivateMessageEntry.class).add(Restrictions.or(c1,c2)).list();
       /* List<HistoryEntryRequest> history = new ArrayList<>();
        // subjects to compare
        String fromEmail = users.get(user_idx).getUsername();
        String toEmail = privateTarget;

        // getting entries needed
        for (HistoryEntryRequest e : h.getPrivateEntries()) {
            String sender = e.getTarget();
            String receiver = e.getOrigin().getUsername();

            if ((sender.equals("@" + fromEmail) && receiver.equals(toEmail)) || (sender.equals("@" + toEmail) && receiver.equals(fromEmail))) {
                e.getReadUser().add(currentUser);
                history.add(e);
            }
        }*/
        return entries;
    }
    //Endpoint to get all group chat history of request group
    @RolesAllowed({"Admin", "User"})
    @Path("/{param}")
    @GET
    @Produces(MediaType.APPLICATION_XML)
    public List<GroupMessageEntry> getGroupHistory(@PathParam("param") String groupName, @Context HttpServletRequest request) {
        User originUser = (User) request.getAttribute("user");
        s.beginTransaction();
        List<ChatGroup> groups =s.createCriteria(ChatGroup.class).add(Restrictions.eq("name", groupName)).list();
        if(groups.size()<1){
            return null;
        }
        ChatGroup targetGroup = groups.get(0);
        if(!targetGroup.getMembers().contains(originUser)){
            return null;
        }
        List<GroupMessageEntry> entries = s.createCriteria(GroupMessageEntry.class).add(Restrictions.eq("target", targetGroup)).list();
        return entries;
    }
    //Get unread condition of user
    /*
    @RolesAllowed({"Admin", "User"})
    @Path("/unread/")
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public Response getUnreadStatus(@Context HttpServletRequest request) {
        int user_idx = (int) request.getAttribute("useridx");
        HashMap<String, Integer> unreadMap = new HashMap<>();
        UserRequest currentUser = users.get(user_idx);
        //Get current user group subscriptions
        Set<GroupRequest> currentUserGroups = currentUser.getSubcriptions();
        int count;
        //loop through these group
        for (GroupRequest g : currentUserGroups) {
            //init the count
            count = 0;
            //loop through group history entries
            for (HistoryEntryRequest e : h.getGroupEntries()) {
                //System.out.println("target:" + e.getTarget());
                //Check if the target of this history entry is the correct group, and if the user already read the entry
                if (e.getTarget().equals(g.getName()) && !e.getReadUser().contains(currentUser)) {
                    //put the group and count of unread entry to the map
                    unreadMap.put(g.getName(), ++count);
                }
            }
            
        }
        //similar to checking unread entry in group entries but this time going through private history entries
        count = 0;
        for (HistoryEntryRequest e : h.getPrivateEntries()) {
            //System.out.println("target:" + e.getTarget());
            
            if (e.getTarget().equals("@" + currentUser.getUsername()) && !e.getReadUser().contains(currentUser)) {
                //Same as checking group entry but put @ in the beginning to know this is private unread
                unreadMap.put("@" + e.getOrigin().getUsername(), ++count);
            }
        }
        //Make the special string with seperator from the map
        String unread = "";
        for (String s : unreadMap.keySet()) {
            unread = unread + s + ":" + unreadMap.get(s) + "|";
        }
        return Response.ok().entity(unread).build();
    }*/

}
