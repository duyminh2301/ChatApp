/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package APIResources;

import Models.ChatGroup;
import Models.GroupMessageEntry;
import Models.PrivateMessageEntry;
import Models.User;
import java.util.List;
import javax.ws.rs.core.Context;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.Suspended;
import javax.ws.rs.core.MediaType;
import RequestSchema.HistoryEntryRequest;
import Util.ApplicationUtil;
import java.util.Calendar;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import javax.annotation.security.RolesAllowed;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

/**
 * REST Web Service
 *
 * @author minhcao
 */
@Path("/chat")
public class ChatResource {

    @Context
    private ServletContext servletContext;

    private ApplicationUtil util = ApplicationUtil.getInstance();
    private Session s = ApplicationUtil.getInstance().getSessionFactory().openSession();


    public ChatResource() {
    }

    /**
     * Retrieves representation of an instance of Resources.ChatResource
     *
     * @param request
     * @param asyncResp
     */
    //Important endpoint for long polling
    //Request is suspended and wait for some event to be resumed
    @RolesAllowed({"Admin", "User"})
    @GET
    public void hangUp(@Context HttpServletRequest request, @Suspended AsyncResponse asyncResp) {
        User u = (User) request.getAttribute("user");
        asyncResp.setTimeout(1, TimeUnit.MINUTES);
        util.addAsync(u.getId(), asyncResp);
       
    }

    //Endpoint to chat private to another user
    @RolesAllowed({"Admin", "User"})
    @Path("/@{param}")
    @POST
    @Consumes(MediaType.APPLICATION_XML)
    @Produces(MediaType.APPLICATION_XML)
    public Response chatToPrivate(HistoryEntryRequest e, @PathParam("param") String targetPrivate, @Context HttpServletRequest request) {
        //long userId = (long) request.getAttribute("userId");
        
       // User originUser = (User) s.get(User.class, userId);
        User originUser = (User) request.getAttribute("user");
        if (targetPrivate.isEmpty() || targetPrivate.trim().isEmpty() || e.getMesssage().isEmpty() || e.getMesssage().trim().isEmpty() || e.getOrigin().getUsername().isEmpty() || !e.getOrigin().getUsername().equals(originUser.getUsername())) {
            return Response.notAcceptable(null).build();
        }
        //find user from database from the name
        User targetUser = this.getTargetUser(targetPrivate);
        //if user does not exist return error
        if (targetUser == null) {
            return Response.notAcceptable(null).build();
        }
        //Create new history entry
        HistoryEntryRequest entry = new HistoryEntryRequest(e.getOrigin(), "", e.getMesssage());
        //notify own
        util.notifyUser(originUser.getId(), e);
        entry.setTarget("@" + targetPrivate);
       //notify target user
        util.notifyUser(targetUser.getId(), entry);
        PrivateMessageEntry messageEntry = new PrivateMessageEntry();
        messageEntry.setMessage(e.getMesssage());
        messageEntry.setOrigin(originUser);
        messageEntry.setTarget(targetUser);
        messageEntry.setTimeCreated(Calendar.getInstance());
        Set<User> readUser = new HashSet<>();
        //readUser.add(originUser);
        messageEntry.setReadUser(readUser);
        s.beginTransaction();
        s.saveOrUpdate(messageEntry);
        s.getTransaction().commit();
        return Response.ok().build();
    }

    @RolesAllowed({"Admin", "User"})
    @Path("/{param}")
    @POST
    @Consumes(MediaType.APPLICATION_XML)
    @Produces(MediaType.APPLICATION_XML)
    public Response chatToGroup(final HistoryEntryRequest e, @PathParam("param") String groupName, @Context HttpServletRequest request) {
        //long userId = (int) request.getAttribute("userId");
        //
        
        //User originUser = (User) s.get(User.class, userId);
        
        User originUser = (User) request.getAttribute("user");
        
        if (groupName.trim().isEmpty() ||e.getMesssage().trim().isEmpty() || e.getOrigin().getUsername().isEmpty() || !e.getOrigin().getUsername().equals(originUser.getUsername())) {
            return Response.notAcceptable(null).build();
        }
        // get groupUser from group name
        s.beginTransaction();
        ChatGroup targetGroup = this.getGroup(groupName);
        if(targetGroup==null){
            return Response.notAcceptable(null).build();
        }
        Set<User> groupUser = targetGroup.getMembers();
        //check if there are no user in group or user not in group
        if (groupUser.size() < 1||!groupUser.contains(originUser)) {
            return Response.notAcceptable(null).build();
        }
        // for sender
        HistoryEntryRequest entry = new HistoryEntryRequest(e.getOrigin(), groupName, e.getMesssage());
        //entry.setTarget(groupName);
        for (User u : groupUser) {
            util.notifyUser(u.getId(), entry);
        }
        //mark origin user as already read this history entry
        GroupMessageEntry messageEntry = new GroupMessageEntry();
        messageEntry.setMessage(e.getMesssage());
        messageEntry.setOrigin(originUser);
        messageEntry.setTarget(targetGroup);
        messageEntry.setTimeCreated(Calendar.getInstance());
        Set<User> readUser = new HashSet<>();
        //readUser.add(originUser);
        messageEntry.setReadUser(readUser);
        s.saveOrUpdate(messageEntry);
        s.getTransaction().commit();
        return Response.ok().build();
    }
/*
    // ---------------------------------------Uploading images-------------------------------------------
    //Endpoint to post image to single user
    @RolesAllowed({"Admin", "User"})
    @Path("/image/@{param}")
    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response uploadFilePrivate(FormDataMultiPart form, @PathParam("param") String targetPrivate, @Context HttpServletRequest request) {
        String fileName = h.saveFile(form,servletContext);
        if (fileName == null) {
            // exception message
            return Response.status(500).entity(fileName).build();
        }
        
        int user_idx = (int) request.getAttribute("useridx");
        User originUser = users.get(user_idx);
        User targetUser = this.getTargetUser(targetPrivate);
        if (targetUser == null) {
            return Response.notAcceptable(null).build();
        }
        
        // for sender
        HistoryEntry entry = new HistoryEntry(originUser, "", "", fileName, "image");
        //notify own
        h.notify(originUser, entry);
        // put @ for client to identify historyentry to group or single user when resume
        entry.setTarget("@"+targetPrivate);
        //notify target user
        h.notify(targetUser, entry);
        entry.getReadUser().add(originUser);
        h.addPrivateEntry(entry);
        h.save();
        return Response.status(200).entity(fileName).build();
    }
    //Endpoint to send image to group .Similar to send message to group above .
    @RolesAllowed({"Admin", "User"})
    @Path("/image/{param}")
    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response uploadFileGroup(FormDataMultiPart form, @PathParam("param") String groupName, @Context HttpServletRequest request) {
        String fileName = h.saveFile(form,servletContext);
        if (fileName == null) {
            // exception message
            return Response.status(500).entity(fileName).build();
        }
        int user_idx = (int) request.getAttribute("useridx");
        User originUser = users.get(user_idx);
        if (groupName.isEmpty() || groupName.trim().isEmpty()) {
            return Response.notAcceptable(null).build();
        }
        
        List<User> groupUser = this.getGroupUser(groupName);
        if (groupUser.size() < 1) {
            return Response.notAcceptable(null).build();
        }

        HistoryEntry entry = new HistoryEntry(originUser, "", "", fileName, "image");
        users.get(user_idx).getAsync().resume(entry);


        entry.setTarget(groupName);
        for (User u : groupUser) {
            h.notify(u, entry);
        }
        entry.getReadUser().add(originUser);
        h.addGroupEntry(entry);
        h.save();
        return Response.status(200).entity(fileName).build();
    }    

    
*/
    private User getTargetUser(String targetName){
        List<User> results =s.createCriteria(User.class).add(Restrictions.eq("username", targetName)).list();
        if(results.size()>0){
            return results.get(0);
        }
        return null;
    }
    private ChatGroup getGroup(String groupName){
        List<ChatGroup> groups =s.createCriteria(ChatGroup.class).add(Restrictions.eq("name", groupName)).list();
        if(groups.size()>0){
            return groups.get(0);
        }
        return null;
    }
}
