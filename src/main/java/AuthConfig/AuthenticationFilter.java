/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AuthConfig;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Priority;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestFilter;
import Models.User;
import Util.ApplicationUtil;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.annotation.security.DenyAll;
import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author minhcao
 */
@Provider
@Priority(Priorities.AUTHENTICATION)
public class AuthenticationFilter implements ContainerRequestFilter {

    @Context
    private ResourceInfo resourceInfo;
    @Context
    private HttpServletRequest request;
    private static final String AUTHORIZATION_PROPERTY = "Authorization";
    private static final String AUTHENTICATION_SCHEME = "";
    
    //method to filter incoming request 
    @Override
    public void filter(ContainerRequestContext requestContext) throws IOException {
        //Get the resource method
        Method method = resourceInfo.getResourceMethod();
        //get the annotation above that method and check is there @PermitAll
        if (method.isAnnotationPresent(PermitAll.class)) {
            return;
        }
        //Access denied for all
        if (method.isAnnotationPresent(DenyAll.class)) {
            requestContext.abortWith(unauthorizedResponse());
            return;
        }
        //Check if there is @RolesAllowed({"Admin", "User"})
        if (method.isAnnotationPresent(RolesAllowed.class)) {
            //Get map from header
            MultivaluedMap<String, String> headers = requestContext.getHeaders();
            //Get the authorization property
            List<String> authorization = headers.get(AUTHORIZATION_PROPERTY);
            //Get the role inside @RolesAllowed({"Admin", "User"}) and put into a set
            RolesAllowed rolesAnnotation = method.getAnnotation(RolesAllowed.class);
            Set<String> rolesSet = new HashSet<>(Arrays.asList(rolesAnnotation.value()));
            //abort request that has no authorization header
            if (authorization == null || authorization.isEmpty()) {
                requestContext.abortWith(unauthorizedResponse());
                return;
            }
            //this is for oauth currently not work
            String token = authorization.get(0).replace(AUTHENTICATION_SCHEME, "");
            if (!validateGoogle(token) && !validateOwnAuth(token, rolesSet)) {
                requestContext.abortWith(unauthorizedResponse());
            }
        }
    }
//Not use right now in application due to Glassfish Jackson problem
    private boolean validateGoogle(String token) {
        try {
            URL url = new URL("https://www.googleapis.com/oauth2/v3/tokeninfo?access_token=" + token);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.connect();

            int code = connection.getResponseCode();
            if (code == 200) {
                return true;
            } else {
                return false;
            }

        } catch (MalformedURLException ex) {
            Logger.getLogger(AuthenticationFilter.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(AuthenticationFilter.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    private Response unauthorizedResponse() {
        return Response.status(Response.Status.UNAUTHORIZED)
                .entity("You cannot access this resource")
                .build();
    }
//method to validate authorization token 
    private boolean validateOwnAuth(final String token, final Set<String> rolesSet) {
        boolean isAllowed = false;

        Session s=ApplicationUtil.getInstance().getSessionFactory().openSession();
        s.beginTransaction();
        List<User> users =s.createCriteria(User.class).add(Restrictions.eq("token", token)).add(Restrictions.in("privilege", rolesSet)).list();
        s.getTransaction().commit();
        if (users.size()>0){
            long userId=users.get(0).getId();
            request.setAttribute("user", users.get(0));
            ApplicationUtil.getInstance().registerOnlineUser(userId);
            isAllowed = true;
        }
       
            
        return isAllowed;
    }

}
