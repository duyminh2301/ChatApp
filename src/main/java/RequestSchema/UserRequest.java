/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RequestSchema;

import java.io.Serializable;
import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;
import javax.ws.rs.container.AsyncResponse;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author beochot
 */
@XmlRootElement
public class UserRequest implements Serializable{
    private String username;
    private String password;
    private String firstName;
    private String lastName;
    private String email;
    private String title;
    private String age;
    private String role;
    private String token;
    private String avatar;

    public UserRequest() {
    }
    public UserRequest(String username) {
        this.username= username; 
    }

    public UserRequest(String username, String password, String role) {
        this();
        this.username = username;
        this.password = password;
        this.role = role;
    }


    public String getUsername() {
        return username;
    }
    
    @XmlElement
    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstName() {
        return firstName;
    }

    @XmlElement
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }
    
    @XmlElement
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAvatar() {
        return avatar;
    }

    @XmlElement
    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getEmail() {
        return email;
    }

    @XmlElement
    public void setEmail(String email) {
        this.email = email;
    }

    public String getTitle() {
        return title;
    }

    @XmlElement
    public void setTitle(String title) {
        this.title = title;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }
    
    public String getPassword() {
        return password;
    }
    @XmlTransient
    public void setPassword(String password) {
        this.password = password;
    }


/*
    public boolean isOnline() {
        return isOnline;
    }
    @XmlTransient
    public void setOnline(boolean isOnline) {
        this.isOnline = isOnline;
    }
 */   
    
    public String getRole() {
        return role;
    }
    @XmlElement
    public void setRole(String role) {
        this.role = role;
    }

    public String getToken() {
        return token;
    }
    @XmlTransient
    public void setToken(String token) {
        this.token = token;
    }

  
    @Override
    public boolean equals(Object other){
        if (other == null) return false;
        if (other == this) return true;
        if (!(other instanceof UserRequest))return false;
        UserRequest otherUser = (UserRequest)other;
        return this.username.equals(otherUser.username);
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 23 * hash + Objects.hashCode(this.username);
       // hash = 23 * hash + Objects.hashCode(this.role);
        return hash;
    }


    
}
