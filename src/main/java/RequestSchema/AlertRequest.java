/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RequestSchema;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author minhcao
 */
@XmlRootElement
public class AlertRequest implements Serializable {

    private int _id;

    private Date time;
    private UserRequest origin;
    private String message;
    private String targetList;
    private Set<UserRequest> confirmList;


    public AlertRequest() {
        this.time = new Date();
        //this._id = History.getInstance().getAlerts().size() + 1;
        this.confirmList = new HashSet<>();
    }

    public AlertRequest(UserRequest origin, String targetList, String message) {
        this();
        this.origin = origin;
        this.targetList = targetList;
        this.message = message;
    }

    public Date getTime() {
        return time;
    }

    @XmlElement
    public void setTime(Date time) {
        this.time = time;
    }

    public UserRequest getOrigin() {
        return origin;
    }

    @XmlElement
    public void setOrigin(UserRequest origin) {
        this.origin = origin;
    }

    public String getTargetList() {
        return targetList;
    }

    @XmlElement
    public void setTargetList(String targetList) {
        this.targetList = targetList;
    }

    public String getMessage() {
        return message;
    }

    @XmlElement
    public void setMessage(String message) {
        this.message = message;
    }

    public int getId() {
        return _id;
    }

    @XmlElement
    public void setId(int _id) {
        this._id = _id;
    }

    public Set<UserRequest> getConfirmList() {
        return confirmList;
    }

    @XmlTransient
    public void setConfirmList(Set<UserRequest> confirmList) {
        this.confirmList = confirmList;
    }
}
