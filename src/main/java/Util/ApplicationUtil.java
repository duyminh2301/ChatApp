/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.ws.rs.container.AsyncResponse;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.tool.hbm2ddl.SchemaExport;

/**
 * Hibernate Utility class with a convenient method to get Session Factory
 * object.
 *
 * @author beochot
 */
public class ApplicationUtil {

    private static ApplicationUtil instance;
    private final SessionFactory sessionFactory;
    private Set<Long> onlineUser;
    private Map<Long,AsyncResponse> asyncMap;
    public ApplicationUtil() {
        onlineUser= new HashSet<>();
        asyncMap = new HashMap<>();
        Configuration config = new Configuration();
        config.configure();
        config.addAnnotatedClass(Models.User.class);
        config.addAnnotatedClass(Models.ChatGroup.class);
        config.addAnnotatedClass(Models.HistoryEntry.class);
        config.addAnnotatedClass(Models.FileEntry.class);
        config.addAnnotatedClass(Models.MessageEntry.class);
        config.addAnnotatedClass(Models.GroupMessageEntry.class);
        config.addAnnotatedClass(Models.PrivateMessageEntry.class);
        config.addAnnotatedClass(Models.UserDetail.class);
        config.addAnnotatedClass(Models.AlertEntry.class);
        new SchemaExport(config).create(true, true);
        StandardServiceRegistryBuilder serviceRegistryBuilder
                = new StandardServiceRegistryBuilder();
        serviceRegistryBuilder.applySettings(config.getProperties());
        ServiceRegistry serviceRegistry = serviceRegistryBuilder.build();
        sessionFactory = config.buildSessionFactory(serviceRegistry);
    }

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }
    public void registerOnlineUser(Long userId){
        onlineUser.add(userId);
    }
    public void addAsync(Long userId,AsyncResponse async){
        onlineUser.add(userId);
        asyncMap.put(userId, async);
    }
    public void removeAsync(Long userId){
        onlineUser.remove(userId);
        asyncMap.remove(userId);
    }
    public void notify(Object o){
        for(Long id : onlineUser){
            asyncMap.get(id).resume(o);
        }
    }
    public void notifyUser(Long userId,Object o){
        AsyncResponse async=asyncMap.get(userId);
            if(async!=null){
                async.resume(o);
            } 
    }
    public static synchronized ApplicationUtil getInstance() {
        if (instance == null) {
            instance = new ApplicationUtil();
        }
        return instance;
    }

}
