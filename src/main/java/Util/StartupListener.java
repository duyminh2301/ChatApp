/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Util;


import Models.ChatGroup;
import Models.FileEntry;
import Models.GroupMessageEntry;
import Models.User;
import Models.UserDetail;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.servlet.ServletContextEvent;
import javax.servlet.annotation.WebListener;
import org.hibernate.Session;

/**
 *
 * @author beochot
 */
@WebListener
public class StartupListener implements javax.servlet.ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        System.out.println("StartupListener contextInitialized()");
        ApplicationUtil.getInstance();
        Session session = ApplicationUtil.getInstance().getSessionFactory().openSession();
        session.beginTransaction();
        User u1 = new User("admin", "admin");
        u1.setPrivilege("Admin");
        User u2 = new User("admin2", "admin2");
        u2.setPrivilege("Admin");
        User u3 = new User("admin3", "admin3");
        u3.setPrivilege("Admin");
        User u4 = new UserDetail("cac","cac","cax@gmail.com","Cuc", "33",null, "hac", "1111");
        User u5 = new UserDetail("cac2","cac2","cax@gmail.com2","Cuc2", "332",null, "hac2", "11111");
        Set<User> members = new HashSet<>();
        members.add(u1);
        members.add(u2);
        members.add(u3);
        ChatGroup group = new ChatGroup("Fapper",members,false);
        FileEntry file = new FileEntry("path","type");
        GroupMessageEntry gentry = new GroupMessageEntry(group, "con cac",file, Calendar.getInstance(),u2, null);
        GroupMessageEntry gentry2 = new GroupMessageEntry(group, "con cac2",file, Calendar.getInstance(),u1, null);
        session.saveOrUpdate(u1);
        session.saveOrUpdate(u2);
        session.saveOrUpdate(u3);
        session.saveOrUpdate(u4);
        session.saveOrUpdate(u5);
        session.saveOrUpdate(group);
        session.saveOrUpdate(file);
        session.saveOrUpdate(gentry);
        session.saveOrUpdate(gentry2);
        session.getTransaction().commit();
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        System.out.println("StartupListener contextDestroyed()");
        ApplicationUtil.getInstance().getSessionFactory().close();
    }

}
