var token = localStorage.getItem("token");
var username = localStorage.getItem('username');
var target = "";
var joinedGroups = [];
var isAdmin;
var userList = [];
var currentGroup;
var userObject;



$(document).ready(function () {

    getUserInfo();
    listJoinedGroups();
    listAllUsers();
    feedMessage();

    //Hide chatbox option
    $("#option-chatbox").hide();

    // Hide post mess
    $('.send-message').hide();
    getAlerts();

    // Send new messages
    $('form.send-message').on('submit', function (event) {
        event.preventDefault();
        var des = $('form.send-message').attr("id");

        var xml = composeMessage();
        callAjax('/ChatApp/app/chat/' + des, "POST", xml, "application/xml", function () {
            $("#message").val("");
        });
    });

    // Log out
    $('#log-out').click(function () {
        //localStorage.clear();
        //window.location = location.origin + '/ChatApp/';
        callAjax('/ChatApp/app/auth/logout/', "GET", null, "application/xml", function () {
            localStorage.clear();
            window.location = location.origin + '/ChatApp/';
        });
    });

    // User profile
    $('#user-profile').click(function () {
        callAjax('/ChatApp/app/user/profile/', "POST", null, "application/xml", function (data) {
            console.log(data);
        });
    });


    $("#openAlert").click(function (e) {
        console.log("in alert", isAdmin);
        if (!isAdmin) {
            alert("Only admins could send alerts !");
            e.stopPropagation();
        }
    });

    $("#sendAlert").click(function () {
        console.log($("#alertList").val());
        var list = "";
        var message = $("#alertMessage").val();
        $("#alertList").val().forEach(function (user) {
            console.log(user);
            list = list + user + ",";
        });

        if (!list || !message) {
            alert("Please fill in required inputs!");
            return;
        }
        // remove last comma
        list = list.substring(0, list.length - 1);
        var xml = "<alert><time>null</time><origin>" + "<username>" + username + "</username>" + "</origin>" + "<targetList>" + list + "</targetList><message>" + message + "</message></alert>";

        // send request
        callAjax('/ChatApp/app/alert', 'POST', xml, 'application/xml', function () {
            alert("Successfully sent alerts!");
            $('#alert-modal').modal('toggle');
        });
    });

    $("#promoteUser").click(function () {
        if (!isAdmin) {
            alert("Only admins are able to promote!");
            return;
        }
        callAjax("/ChatApp/app/user/promote/", "POST", "<user>" + "<username>" + $("select[id=promoteList]").val() + "</username>" + "</user>", "application/xml", function () {
            alert("Successfully promoted!");
            $('#user-mgmt-modal').modal('toggle');
        });
    });

    // Add users to group
    $("#addUserToGroup").click(function () {
        var inviteUsers = $("#addList").val();
        var invite_users_xml = "";
        if (inviteUsers) {
            inviteUsers.forEach(function (user) {
                invite_users_xml += "<user><username>" + user + "</username></user>"
            });
        } else {
            invite_users_xml = null;
            alert("Please select at least 1 user!");
            return;
        }
        group = $(".group").attr("href").slice(1);
        callAjax("/ChatApp/app/group/addUser/" + currentGroup, "POST", "<users>" + invite_users_xml + "</users>", "application/xml", function () {
            alert("Successfully added!");
            $('#add-modal').modal('toggle');
        });
    });

    // User profile
    $("#profile-form").submit(function (e) {
        var fname = $("#profile-fname").val();
        var lname = $("#profile-lname").val();
        var title = $("#profile-title").val();
        var email = $("#profile-email").val();
        var avatar = $(".avatar").attr("src");
        if(avatar === "img/img-placeholder.png"){
            avatar = "";
        }
        console.log(avatar);
        e.preventDefault();
        if (!fname || !lname || !title || !email) {
            $("#profile-alert").show().fadeOut(1000);
            return;
        }
        callAjax("/ChatApp/app/user/profile", "POST", "<user><username>" + username + "</username><firstName>" + fname + "</firstName><lastName>" + lname + "</lastName><title>" + title + "</title><email>" + email + "</email><avatar>" + avatar + "</avatar></user>", "application/xml", function () {
            alert("Successfully added!");
            $('#profile-modal').modal('toggle');
        });
    });

// Polling messages
    function feedMessage() {
        $.ajax({
            url: "/ChatApp/app/chat",
            method: "GET",
            dataType: "xml",
            statusCode: {
                401: function (response) {
                    localStorage.clear();
                    window.location.replace("index.html");
                },
                500: function (response) {
                    localStorage.clear();
                    window.location.replace("index.html");
                }
            },
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Authorization", token);
            },
            success: function (result) {
                // Play sound
                playSound('/ChatApp/sounds/notification-sound');

                // Handle new mess
                updateNewMess(result);
                feedMessage();
            },
            error: function () {
                feedMessage();
            }
        });
    }

// Play sound
    function playSound() {
        return $(
                '<audio autoplay="autoplay" style="display:none;">'
                + '<source src="' + arguments[0] + '.mp3" />'
                + '<source src="' + arguments[0] + '.ogg" />'
                + '<embed src="' + arguments[0] + '.mp3" hidden="true" autostart="true" loop="false" class="playSound" />'
                + '</audio>'
                ).appendTo('body');
    }

// Do Ajax
    function callAjax(url, method, data, contentType, callback) {
        $.ajax({
            url: url,
            method: method,
            contentType: contentType,
            data: data,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Authorization", token);
            },
            success: function (result) {
                callback(result);
            }
        });
    }

// Show All Users
    function listAllUsers() {
        callAjax("/ChatApp/app/user/", "GET", null, "application/xml", updateUser);
    }

// Get user's info
    function getUserInfo() {
        callAjax("/ChatApp/app/user/profile", "GET", null, "application/xml", function (data) {
            console.log(data);
            var firstName, lastName, title, email, avatar, role, username, fullName, displayName;
            $(data).find("user").each(function () {
                firstName = $(this).find("firstName").text();
                username = $(this).find("username").text();
                lastName = $(this).find("lastName").text();
                title = new Date($(this).find("title").text());
                email = $(this).find("email").text();
                avatar = $(this).find("avatar").text();
                role = $(this).find("role").text();
                fullName = firstName + " " + lastName;
                displayName = fullName.trim() ? fullName : username;
            });
            userObject = {
                username: username || "",
                firstName: firstName || "",
                lastName: lastName || "",
                title: title || "",
                email: email || "",
                avatar: avatar || "",
                role: role,
                displayName: displayName
            };
            isAdmin = role === "Admin";

            //Insert Username
            if (firstName && lastName) {
                displayName = displayName + " (" + username + ")";
                $('#username').html(displayName);

            } else {
                $('#username').html(username);
            }
        });
    }

// List all users in ul
    function updateUser(data) {
        $("ul#users").html(" ");
        $('ul#users').append("\
            <button class='btn btn-default' style='' id='user-mgmt'\
                data-toggle='modal' data-target='#user-mgmt-modal'>\
                Management\
            </button>");
        $("#user-mgmt").click(function (e) {
            console.log("in user");
            if (!isAdmin) {
                alert("Only admins could manage users !");
                e.stopPropagation();
            }
        });
        $(data).find("user").each(function () {
            var firstName = $(this).find("firstName").text();
            var uname = $(this).find("username").text();
            var lastName = $(this).find("lastName").text();
            var fullName = firstName + " " + lastName;
            var displayName = fullName.trim() ? fullName : uname;
            displayName = displayName + " (" + uname + ")";
            if (uname !== username) {
                userList.push(uname);
                $("select[name=userlist]").append("<option value='" + uname + "'>" + displayName + "</option>");
                $("ul#users").append("<li><a href='#" + uname + "' class='user'>" + displayName + " <span class='badge'></span></a></li>");
            }
        });

        $("select[name=userlist]").multiselect({maxHeight: 200});

        $('.user').click(function () {
            $(".navbar-collapse").collapse('hide');
            getPrivateHistory($(this).attr("href").slice(1), $(this).text());
        });
        unreadMess();
    }

// Show Groups Users joined
    function listJoinedGroups() {
        callAjax("/ChatApp/app/group/", "GET", null, "application/xml", updateGroup);
    }

// List all joined allGroups in ul
    function updateGroup(data) {
        $("ul#groups").html(" ");
        $('ul#groups').append("\
            <button class='btn btn-default' style='' id='join-modal'\
                data-toggle='modal' data-target='#join-group'>\
                Management\
            </button>");
        joinedGroups = [];
        $(data).find("name").each(function () {
            joinedGroups.push(this.innerHTML);
            $("ul#groups").append("<li><a href='#" + this.innerHTML + "' class='group'>" + this.innerHTML + " <span class='badge'></span></a></li>");
        });

        $('.group').click(function () {
            $(".navbar-collapse").collapse('hide');
            getGroupHistory($(this).attr("href").slice(1));
            currentGroup = $(this).attr("href").slice(1);
        });

        $('#join-modal').click(function () {
            listAllGroups();
        });

        unreadMess();
    }

// Check joined allGroups
    function checkJoined(group) {
        return joinedGroups.some(function (elem) {
            return group === elem;
        });
    }

// Show ALL Groups
    function listAllGroups() {
        callAjax("/ChatApp/app/group/all", "GET", null, "application/xml", updateAllGroup);
    }

// GET groups info
    function showGroups(groupName, quantity) {
        var list = "Members:<br>";
        callAjax("/ChatApp/app/group/" + groupName, "GET", null, "application/xml", function (result) {
            $(result).find("username").each(function () {
                list += $(this).text() + "<br>";
            });

            if (!checkJoined(groupName)) {
                $("ul#all-group").append("\
                <li class='left clearfix'>\
                    <div class='chat-body clearfix'>\
                        <div class='header'>\
                            <strong class=primary-font'>" + groupName + "</strong>\
                            \
                            <div class='pull-right text-muted'>\
                                <a href='#join-" + groupName + "' class='btn btn-danger join-group'>Join</a>\
                            </div>\
                        </div>\
                        \
                        <a href='#' data-toggle='tooltip' data-placement='right' data-original-title='" + list + "'>\
                            <i class='fa fa-users'></i>" + quantity + "\
                        </a>\
                    </div>\
                </li>");
            } else {
                $("ul#all-group").append("\
                <li class='left clearfix'>\
                    <div class='chat-body clearfix'>\
                        <div class='header'>\
                            <strong class=primary-font'>" + groupName + "</strong>\
                            \
                            <div class='pull-right text-muted'>\
                                <a href='#leave-" + groupName + "' class='btn btn-warning leave-group'>Leave</a>\
                            </div>\
                        </div>\
                        \
                        <a href='#' data-toggle='tooltip' data-placement='right' data-original-title='" + list + "'>\
                            <i class='fa fa-users'></i>" + quantity + "\
                        </a>\
                    </div>\
                </li>");
            }

            // Join group Event listener
            $('.join-group').click(function () {
                joinGroup($(this).attr("href").slice(6));
            });

            // Leave group Event listener
            $('.leave-group').click(function () {
                leaveGroup($(this).attr("href").slice(7));
            });

            // Show group members
            $('[data-toggle="tooltip"]').tooltip({html: true});
        });
    }


// List all group to JOIN in ul
    function updateAllGroup(data) {
        $('#group-modal-title').html("Group management");
        $("ul#all-group").html(" ");
        $('#group-modal-footer').html("<button class='btn btn-success' id='add-group-btn'>Add new group</button>");

        $(data).find("group").each(function () {
            var groupName = $(this).find("name").text();
            var quantity = $(this).find("size").text();
            showGroups(groupName, quantity);
        });

        // Add new group
        addGroup();
    }

// Join group
    function joinGroup(group) {
        callAjax("/ChatApp/app/group/join/", "POST", "<group><name>" + group + "</name></group>", "application/xml",
                function () {
                    listJoinedGroups();
                    listAllGroups();
                });
        event.preventDefault();

    }

// Leave group
    function leaveGroup(group) {
        callAjax("/ChatApp/app/group/leave/", "POST", "<group><name>" + group + "</name></group>", "application/xml",
                function () {
                    listJoinedGroups();
                    listAllGroups();
                });
        event.preventDefault();
    }

// Add messages in to chat div
    function updateHistory(data) {
        $('.send-message').show();
        $(data).find("historyEntry").each(function () {
            var message = $(this).find("message").text();
            var username = $(this).find("username").text();
            var time = new Date($(this).find("timeCreated").text());
            var filePath = $(this).find("filePath").text();
            var fileType = $(this).find("fileType").text();
            var firstName = $(this).find("firstName").text();
            var lastName = $(this).find("lastName").text();
            var fullName = firstName + " " + lastName;
            var displayName = fullName.trim() ? fullName : username;
            if (fileType === 'image') {
                var imgMess = "<img src='images/" + filePath + "'/>";
                $("ul#chat").append(newMessage(displayName, formatTime(time), imgMess));
            } else {
                $("ul#chat").append(newMessage(displayName, formatTime(time), message));
            }

        });
        $("html, body").animate({scrollTop: $('#chat').height()}, 500);
    }

// Pooling new message
    function updateNewMess(data) {
        if ($(data).find("alert").length > 0) {
            displayAlert(data);
            return;
        }
        

        $(data).find("historyEntry").each(function () {
            var message = $(this).find("messsage").text();
            var from = $(this).find("username").text();
            var time = new Date($(this).find("time").text());
            var to = $(this).find("target").text();
            var filePath = $(this).find("filePath").text();
            var fileType = $(this).find("fileType").text();
            if(to==="System"){
                if(message==="New Group"){
                    listJoinedGroups();
                }else if(message==="New User"){
                    listAllUsers();
                }
            }
            if (fileType === 'image') {
                var imgMess = "<img src='images/" + filePath + "'/>";
                if (!to) {
                    $("ul#chat").append(newMessage(from, formatTime(time), imgMess));
                }

                if (to.startsWith("@")) { //private chat
                    if (to.slice(1) === username) {
                        if (from === target.slice(1)) {
                            $("ul#chat").append(newMessage(from, formatTime(time), imgMess));
                        } else {
                            //badge into user that send mess
                            var newMessCount = $("a.user[href='#" + from + "'] > span.badge").html();
                            if (newMessCount === "") {
                                $("a.user[href='#" + from + "'] > span.badge").html("1");
                            } else {
                                newMessCount = parseInt(newMessCount);
                                newMessCount += 1;
                                $("a.user[href='#" + from + "'] > span.badge").html(newMessCount.toString());
                            }
                        }
                    }
                } else { // group chat
                    if (to === target) {
                        $("ul#chat").append(newMessage(from, formatTime(time), imgMess));
                    } else {
                        //badge into group that send mess
                        newMessCount = $("a.group[href='#" + to + "'] > span.badge").html();
                        if (newMessCount === "") {
                            $("a.group[href='#" + to + "'] > span.badge").html("1");
                        } else {
                            newMessCount = parseInt(newMessCount);
                            newMessCount += 1;
                            $("a.group[href='#" + to + "'] > span.badge").html(newMessCount.toString());
                        }
                    }
                }

            } else {

                if (!to) {
                    $("ul#chat").append(newMessage(from, formatTime(time), message));
                } else {
                    if (to.startsWith("@")) { //private chat
                        if (to.slice(1) == username) {
                            if (from === target.slice(1)) {
                                $("ul#chat").append(newMessage(from, formatTime(time), message));
                            } else {
                                //badge into user that send mess
                                var newMessCount = $("a.user[href='#" + from + "'] > span.badge").html();
                                if (newMessCount === "") {
                                    //console.log("Current: 0");
                                    $("a.user[href='#" + from + "'] > span.badge").html("1");
                                } else {
                                    newMessCount = parseInt(newMessCount);
                                    newMessCount += 1;
                                    $("a.user[href='#" + from + "'] > span.badge").html(newMessCount.toString());
                                }
                            }
                        }
                    } else { // group chat
                        if (to == target) {
                            $("ul#chat").append(newMessage(from, formatTime(time), message));
                        } else {
                            //badge into group that send mess
                            newMessCount = $("a.group[href='#" + to + "'] > span.badge").html();
                            if (newMessCount === "") {
                                $("a.group[href='#" + to + "'] > span.badge").html("1");
                            } else {
                                newMessCount = parseInt(newMessCount);
                                newMessCount += 1;
                                $("a.group[href='#" + to + "'] > span.badge").html(newMessCount.toString());
                            }
                        }
                    }
                }
            }
        });
        $("html, body").animate({scrollTop: $('#chat').height()}, 500);
    }

// Load Private Message
    function getPrivateHistory(user, displayName) {
        target = "@" + user;
        $("a.user[href='#" + user + "'] span").html("");
        $('.panel-heading h3').html("User " + displayName);
        $('form.send-message').attr("id", "@" + user);
        $("ul#chat").html("");
        callAjax("/ChatApp/app/history/@" + user, "GET", null, "application/xml", updateHistory);
    }

// Load Group Message
    function getGroupHistory(group) {
        target = group;
        $("a.group[href='#" + group + "'] span").html("");
        $('.panel-heading h3').html("Group " + group);
        $("#option-chatbox").show();
        $('form.send-message').attr("id", group);
        $("ul#chat").html("");
        callAjax("/ChatApp/app/group/" + group, "GET", null, "application/xml", function (result) {
            var list = "Members:<br>";
            $(result).find("username").each(function () {
                list += $(this).text() + "<br>";
                
            });
            $('.panel-heading h3').html("<a href='#' data-toggle='tooltip' data-placement='right' data-original-title='" + list + "'>Group "+group+"</a>");
            $('[data-toggle="tooltip"]').tooltip({html: true});
        });
        callAjax("/ChatApp/app/history/" + group, "GET", null, "application/xml", updateHistory);
    }

// Format time to display
    function formatTime(time) {
        var today = new Date();
        var date = time.getDate();
        var month = parseInt(time.getMonth()) + 1;

        if (today.getDate() !== date) {
            return formatDigits(date) + "/" + formatDigits(month);
        } else {
            return formatDigits(time.getHours()) + ":" + formatDigits(time.getMinutes());
        }
    }

// Format 2 digits
    function formatDigits(number) {
        if (number <= 9) {
            return "0" + number.toString();
        } else {
            return number.toString();
        }
    }

// Prepare li to add into chat div
    function newMessage(username, time, message) {
        if (username === "System") {
            return "<li class='left clearfix'>\
                <span class='chat-img pull-left'>\
                    <i class='fa fa-3x fa-laptop'></i>\
                </span>\
                \
                <div class='chat-body clearfix'>\
                    <div class='header'>\
                        <strong class='primary-font'>" + username + "</strong>\
                        <small class='pull-right text-muted'>\
                            <span class='fa fa-clock-o'></span>" + time.toString() + "\
                        </small>\
                    </div>\
                    \
                    <p><i>" + message + "</i></p>\
                </div>\
            </li>";
        }
        return "<li class='left clearfix'>\
                <span class='chat-img pull-left'>\
                    <img src='/ChatApp/img/usr/" + username[0].toUpperCase() + ".png' alt='" + username + "' class='img-circle'/>\
                </span>\
                \
                <div class='chat-body clearfix'>\
                    <div class='header'>\
                        <strong class='primary-font'>" + username + "</strong>\
                        <small class='pull-right text-muted'>\
                            <span class='fa fa-clock-o'></span>" + time.toString() + "\
                        </small>\
                    </div>\
                    \
                    <p>" + message + "</p>\
                </div>\
            </li>";
    }

// Add new group
    function addGroup() {
        $('#add-group-btn').click(function () {
            $('#group-modal-title').html("Create a new group");
            $('ul#all-group').html("\
            <form id='add-group-form'>\
                \
                <div class='form-group'>\
                    <label for='checkbox'>Group type</label>\
                    <div class='onoffswitch'>\
                        <input type='checkbox' name='onoffswitch' class='onoffswitch-checkbox' id='myonoffswitch' checked>\
                        <label class='onoffswitch-label' for='myonoffswitch'>\
                            <span class='onoffswitch-inner'></span>\
                            <span class='onoffswitch-switch'></span>\
                        </label>\
                    </div>\
                </div>\
                \
                <div class='form-group'>\
                    <label for='group-name'>Group name</label>\
                    <input id='group-name' class='form-control' type='text' placeholder='Enter group name here'>\
                </div>\
                \
                <div class='form-group'>\
                    <label for='invite-people'>Invite others to join (optional)</label>\
                    <select id='invite-people' name='invite-people' multiple='multiple'></select>\
                </div>\
            </form>\
            ");

            $('#group-modal-footer').html("<button id='create-group' type='submit' class='btn btn-danger'>Create group</button>");

            var isPrivate = false;
            userList.forEach(function (user) {
                $("#invite-people").append("<option value='" + user + "'>" + user + "</option>");
            });
            $("#invite-people").multiselect({maxHeight: 200});

            $('#myonoffswitch').click(function () {
                isPrivate = !isPrivate;
            });

            $('#create-group').click(function () {
                var newGroup = $('#group-name').val();

                var inviteUsers = $("select[name=invite-people]").val();
                var invite_users_xml = "";
                if (inviteUsers) {
                    inviteUsers.forEach(function (user) {
                        invite_users_xml += "<user><username>" + user + "</username></user>"
                    });
                } else {
                    invite_users_xml = null;
                }
                if (isPrivate) {
                    createPrivateGroup(newGroup, invite_users_xml);
                } else {
                    createPublicGroup(newGroup, invite_users_xml);
                }

                event.preventDefault();
            });
        });
    }

// Create new public group
    function createPublicGroup(group, inviteUsers) {
        callAjax("/ChatApp/app/group/createPublic/", "POST", "<group><name>" + group + "</name></group>", "application/xml",
                function () {
                    if (inviteUsers) {
                        callAjax("/ChatApp/app/group/addUser/" + group, "POST", "<users>" + inviteUsers + "</users>", "application/xml",
                                function () {
                                    listJoinedGroups();
                                    listAllGroups();
                                });
                    } else {
                        listJoinedGroups();
                        listAllGroups();
                    }

                });
    }

// Create new private group
    function createPrivateGroup(group, inviteUsers) {

        callAjax("/ChatApp/app/group/createPrivate/", "POST", "<group><name>" + group + "</name></group>", "application/xml",
                function () {
                    // Select people to join this group
                    //joinGroup(group);
                    if (inviteUsers) {
                        callAjax("/ChatApp/app/group/addUser/" + group, "POST", "<users>" + inviteUsers + "</users>", "application/xml",
                                function () {
                                    // Select people to join this group
                                    //joinGroup(group);
                                    listJoinedGroups();
                                    listAllGroups();
                                });
                    } else {
                        listJoinedGroups();
                        listAllGroups();
                    }

                });
    }

// Prepare xml to POST
    function composeMessage() {
        return '<historyEntry><origin><username>' + username + '</username></origin><messsage>' + $("#message").val() + '</messsage><time>null</time></historyEntry>';

    }

// Unread mess
    function unreadMess() {
        callAjax("/ChatApp/app/history/unread", "GET", null, "application/xml", function (data) {
            var unreads = data.split("|");
            for (var idx in unreads) {
                if (unreads[idx] !== "") {
                    var info = unreads[idx].split(":");
                    if (info[0].startsWith("@")) {
                        $("a.user[href='#" + info[0].substr(1) + "'] > span.badge").html(info[1]);
                    } else {
                        $("a.group[href='#" + info[0] + "'] > span.badge").html(info[1]);
                    }
                }
            }
        });
    }


// Get alerts when logging in
    function getAlerts() {
        callAjax("/ChatApp/app/alert", "GET", null, "application/xml", displayAlert);
    }

// Display alert
    function displayAlert(alerts) {
        //console.log("display", alerts);
        if (!alerts) {
            return;
        }
        $(alerts).find("alert").each(function (index) {

            var id = $(this).find("id").text();
            var message = $(this).find("message").text();
            var from = $(this).find("origin").text();
            var time = new Date($(this).find("time").text());
            $("#dialogs").append("<div id='dialog-" + index + "' title='Alert'><span class='ui-state-default'><span class='ui-icon ui-icon-info' style='float:left; margin:0 7px 0 0;'></span></span><p id='dialog-message-" + index + "'></p></div>");
            $("#dialog-message-" + index).html("At: " + time + "<br>" + "From: " + from + "<br>Message: " + message);
            $("#dialog-" + index).dialog({
                modal: true,
                draggable: false,
                resizable: false,
                show: 'blind',
                hide: 'blind',
                width: 400,
                buttons: [
                    {
                        text: "Confirm that i have read",
                        icons: {
                            primary: "ui-icon-check"
                        },
                        click: function () {
                            var $this = $(this);
                            callAjax("/ChatApp/app/alert/" + id, "POST", null, null, function () {
                                $this.dialog("close");
                            });
                        }
                    }
                ]
            });
        });
    }
}
);
// Sending images
function sendImage() {

    if (!target) {
        return;
    }
    var imgURL = "/ChatApp/app/chat/image/" + target;
    var file = $('#send-image').get(0).files[0];
    var formData = new FormData();
    formData.append('file', file);
    $.ajax({
        type: 'POST',
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Authorization", token);
        },
        url: imgURL,
        data: formData,
        cache: false,
        contentType: false,
        processData: false
    });
}
// Upload avt
function uploadAvt() {
    var imgURL = "/ChatApp/app/user/image/";
    var file = $('#upload-avt').get(0).files[0];
    var formData = new FormData();
    formData.append('file', file);
    $.ajax({
        type: 'POST',
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Authorization", token);
        },
        url: imgURL,
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        success: function (result) {
            $(".avatar").attr("src", "images/"+result);
        }
    });
}
function chooseFile() {
    $("#send-image").click();
}

function chooseProfileImage() {
    $('#upload-image').click();
}

function changeProfileImage(event) {
    $('#profile-image').attr('src', URL.createObjectURL(event.target.files[0]));
}
